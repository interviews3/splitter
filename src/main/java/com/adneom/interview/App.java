package com.adneom.interview;

import com.adneom.interview.splitter.SplitterIntImpl;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 * @author ibrahim ELH
 */
public class App {

    private static final SplitterIntImpl splitter = new SplitterIntImpl();

    /**
     * App entry point
     * Establishment adn disposal of inputs/outputs are the responsibility of this method
     *
     * Input Format
     * The first line contains an integer, the maximum size of sub-lists
     * The second line contains a single string of integers separated by space which represents the list to split
     * Sample: 3
     *         1 2 3 4 5 6 7 8 9 0
     *
     * Output Format
     * Print extracted sub-lists, each one on a line
     * Sample: 1 2 3
     *         4 5 6
     *         7 8 9
     *         0
     */
    public static void main(String[] args) throws IOException {

        Scanner scanner = new Scanner(System.in);
        Writer writer = new PrintWriter(System.out);

        writer.write("WELCOME ON THE LIST SPLITTER\n");
        writer.write("Notice: This version support only string of integers\n");
        writer.write("Input Format:\n");
        writer.write("\tThe first line contains an integer, the maximum size of sub-lists\n");
        writer.write("\tThe second line contains a single string of integers separated by space\n");
        writer.write(String.format("To quit the program type %d\n", 0));
        writer.flush();

        try {
            while (true) {
                interactiveSplit(scanner, writer);
            }
        } catch (NumberFormatException ignored) {
            writer.write("Malformed Input: Should be integers separated with space\n");
            writer.flush();
        } catch (RuntimeException e) {
            writer.write("BYE!\n");
            writer.flush();
            System.exit(0);
        } finally {
            scanner.close();
            writer.close();
        }
    }

    /**
     * Split list of integers interactively
     * @param scanner user input reader
     * @param writer output printer
     * @throws IOException in case of any input or output error
     * @throws RuntimeException if the execution is aborted
     */
    public static void interactiveSplit(Scanner scanner, Writer writer) throws NumberFormatException, IOException, RuntimeException {

        int input = scanner.nextInt();
        if (input == 0)
            throw new RuntimeException("Program Aborted");

        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        List<Integer> integerList = Arrays.stream(scanner.nextLine().split(" ")).map(Integer::parseInt).collect(Collectors.toList());
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        List<List<Integer>> result = splitter.partition(integerList, input);

        for (List<Integer> list : result) {
            writer.write(list.stream().map(Object::toString).collect(Collectors.joining(" ")) + "\n");
        }
        writer.write("\n");
        writer.flush();
    }
}
