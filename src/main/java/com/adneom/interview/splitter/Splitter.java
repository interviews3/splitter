package com.adneom.interview.splitter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ibrahim ELH
 * @param <T> type of collection elements
 */
public interface Splitter<T> {

    /**
     * Cut a list into sub-lists based on a given size
     *
     * @param liste list to cut
     * @param taille maximum size of a sub-list
     * @return resulted sub-lists
     */
    default List<List<T>> partition(List<T> liste, Integer taille) {

        if (liste == null)
            throw new IllegalArgumentException("List cannot be null");
        if (liste.isEmpty())
            throw new IllegalArgumentException("List cannot be empty");
        if (taille == null)
            throw new IllegalArgumentException("Size cannot be null");
        if (taille == 0)
            throw new IllegalArgumentException("Size cannot be 0");
        if (taille > liste.size())
            throw new IllegalArgumentException("Size cannot be greater than the list size");

        List<List<T>> result = new ArrayList<>();

        int from = 0;
        int to = taille;

        while (from < liste.size()) {
            result.add(liste.subList(from, to));
            from = from + taille;
            to = Math.min((to + taille), liste.size());
        }

        return result;
    }
}
