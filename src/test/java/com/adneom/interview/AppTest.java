package com.adneom.interview;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.IOException;
import java.io.Writer;
import java.util.Scanner;

import static com.adneom.interview.App.interactiveSplit;
import static org.mockito.Mockito.*;

/**
 * @author ibrahim
 * Unit tests for the {@link App} class.
 */
@RunWith(JUnit4.class)
public class AppTest {

    private Scanner mockScanner = mock(Scanner.class);
    private Writer mockWriter = mock(Writer.class);

    @Test(expected = RuntimeException.class)
    public final void shouldQuit() throws IOException {

        when(mockScanner.nextInt()).thenReturn(0);

        interactiveSplit(mockScanner, mockWriter);
    }

    @Test
    public final void shouldTakeUserInput() throws IOException {

        when(mockScanner.nextInt()).thenReturn(1);
        when(mockScanner.nextLine()).thenReturn("1 2 3 4 5 6 7 8 9 0");

        interactiveSplit(mockScanner, mockWriter);

        verify(mockScanner, times(1)).nextInt();
        verify(mockScanner, times(1)).nextLine();
        verify(mockWriter, times(11)).write(anyString()); // print 10 sub-lists of 1 element
        verify(mockWriter, times(1)).flush();
    }

    @Test(expected = NumberFormatException.class)
    public final void shouldRejectMalformedUserInput() throws IOException {

        when(mockScanner.nextInt()).thenReturn(1);
        when(mockScanner.nextLine()).thenReturn("1  2  3 4 5 6 7  8 9  0");

        interactiveSplit(mockScanner, mockWriter);
    }
}
