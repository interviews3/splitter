package com.adneom.interview;

import com.adneom.interview.splitter.SplitterIntImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author ibrahim
 * Unit tests for the {@link SplitterIntImpl} class.
 */
@RunWith(JUnit4.class)
public final class SplitterIntImplTest {

    private final SplitterIntImpl splitter =  new SplitterIntImpl();

    @Test(expected = IllegalArgumentException.class)
    public final void whenTheListIsNullThenExceptionIsThrown() {
        splitter.partition(null, 2);

    }

    @Test(expected = IllegalArgumentException.class)
    public final void whenTheListIsEmptyThenExceptionIsThrown() {
        splitter.partition(Collections.emptyList(), 2);

    }

    @Test(expected = IllegalArgumentException.class)
    public final void whenTheSizeIsNullThenExceptionIsThrown() {
        splitter.partition(Arrays.asList(1,2,3), null);
    }

    @Test(expected = IllegalArgumentException.class)
    public final void whenTheSizeIsEqualToZeroThenExceptionIsThrown() {
        splitter.partition(Arrays.asList(1,2,3), 0);

    }

    @Test(expected = IllegalArgumentException.class)
    public final void whenTheSizeExceedsTheListSizeThenExceptionIsThrown() {
        splitter.partition(Arrays.asList(1,2,3), 5);
    }

    @Test
    public final void whenTheSizeIsLessOrEqualToTheListSizeThenNoExceptionIsThrown() {
        assertNotNull(splitter.partition(Arrays.asList(1,2,3), 1));
    }

    @Test
    public final void givenListOf4ElementsAndSizeOf2ThenListOf2SubListsEachOf2ElementsIsReturned() {
        List<Integer> integerList = Arrays.asList(1,2,3,4);
        List<List<Integer>> subLists = splitter.partition(integerList, 2);
        assertNotNull(subLists);
        assertEquals(2, subLists.size());
        assertEquals(2, subLists.get(0).size());
        assertEquals(2, subLists.get(1).size());
        assertEquals(integerList.get(0), subLists.get(0).get(0));
        assertEquals(integerList.get(1), subLists.get(0).get(1));
        assertEquals(integerList.get(2), subLists.get(1).get(0));
        assertEquals(integerList.get(3), subLists.get(1).get(1));
    }

    @Test
    public final void givenListOf7ElementsAndSizeOf3ThenListOf2SubListsOf3ElementsAndOneOf1ElementIsReturned() {
        List<Integer> integerList = Arrays.asList(1,2,3,4,5,6,7);
        List<List<Integer>> subLists = splitter.partition(integerList, 3);
        assertNotNull(subLists);
        assertEquals(3, subLists.size());
        assertEquals(3, subLists.get(0).size());
        assertEquals(3, subLists.get(1).size());
        assertEquals(1, subLists.get(2).size());
        assertEquals(integerList.get(0), subLists.get(0).get(0));
        assertEquals(integerList.get(1), subLists.get(0).get(1));
        assertEquals(integerList.get(2), subLists.get(0).get(2));
        assertEquals(integerList.get(3), subLists.get(1).get(0));
        assertEquals(integerList.get(4), subLists.get(1).get(1));
        assertEquals(integerList.get(5), subLists.get(1).get(2));
        assertEquals(integerList.get(6), subLists.get(2).get(0));
    }

    @Test
    public final void givenListOf100ElementsAndSizeOf1ThenListOf100SubListsOf1ElementIsReturned() {
        List<Integer> integerList = Arrays.asList(1,2,3,4,5,1,2,3,4,5,1,2,3,4,5,1,2,3,4,5,1,2,3,4,5,1,2,3,4,5,1,2,3,4,5,1,2,3,4,5,1,2,3,4,5,
                1,2,3,4,5,1,2,3,4,5,1,2,3,4,5,1,2,3,4,5,1,2,3,4,5,1,2,3,4,5,1,2,3,4,5,1,2,3,4,5,1,2,3,4,5,1,2,3,4,5,1,2,3,4,5,1,2,3);
        List<List<Integer>> subLists = splitter.partition(integerList, 1);
        assertNotNull(subLists);
        assertEquals(103, subLists.size());
        assertEquals(1, subLists.get(0).size());
        assertEquals(1, subLists.get(54).size());
        assertEquals(1, subLists.get(99).size());
        assertEquals(integerList.get( 0), subLists.get( 0).get(0));
        assertEquals(integerList.get( 4), subLists.get( 4).get(0));
        assertEquals(integerList.get( 9), subLists.get( 9).get(0));
        assertEquals(integerList.get(14), subLists.get(14).get(0));
        assertEquals(integerList.get(19), subLists.get(19).get(0));
        assertEquals(integerList.get(24), subLists.get(24).get(0));
        assertEquals(integerList.get(29), subLists.get(29).get(0));
        assertEquals(integerList.get(34), subLists.get(34).get(0));
        assertEquals(integerList.get(39), subLists.get(39).get(0));
        assertEquals(integerList.get(44), subLists.get(44).get(0));
        assertEquals(integerList.get(49), subLists.get(49).get(0));
        assertEquals(integerList.get(54), subLists.get(54).get(0));
        assertEquals(integerList.get(59), subLists.get(59).get(0));
        assertEquals(integerList.get(64), subLists.get(64).get(0));
        assertEquals(integerList.get(69), subLists.get(69).get(0));
        assertEquals(integerList.get(74), subLists.get(74).get(0));
        assertEquals(integerList.get(79), subLists.get(79).get(0));
        assertEquals(integerList.get(84), subLists.get(84).get(0));
        assertEquals(integerList.get(89), subLists.get(89).get(0));
        assertEquals(integerList.get(94), subLists.get(94).get(0));
        assertEquals(integerList.get(99), subLists.get(99).get(0));
    }
}
