# List Splitter

Cut a list into sub-lists based on a given size

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Building or running this project requires that [Java 8 or newer](https://www.oracle.com/technetwork/java/javase/overview/index.html) be available on your machine.

## Development

### Setup

After installing Java, you should be able to run the following command to install project dependencies.

    ./mvnw -DskipTests clean install

You will only need run this command when dependencies change in [pom.xml](../splitter/pom.xml).

Run the following command in a terminal to start the application on the development mode.

    ./mvnw -DskipTests exec:java

## Building for production

To prepare the splitter application for production, run:

    ./mvnw -DskipTests clean package

This will produce a JAR file on the `target` directory. To ensure everything worked, run:

    java -jar target/*.jar

Then you can use the program from your terminal.

## Testing

To launch your application's tests, run:

    ./mvnw clean test

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Java](https://www.oracle.com/technetwork/java/javase/overview/index.html) - The programming language
* [Maven](https://maven.apache.org/) - Dependency Management
* [JUnit](https://junit.org/) and [Mockito](https://site.mockito.org/) - Used to create tests
* [Git](https://git-scm.com/) - as version-control system
* [IntelliJ IDEA](https://www.jetbrains.com/idea/) - as an IDE

## Versioning

We use [SemVer](http://semver.org/) for versioning.

## Author

* **Ibrahim El Hadeg**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
